

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>



	    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div id="app">
	<h2 v-text="message"></h2>

</div>


<script src="https://unpkg.com/vue@2.5.13/dist/vue.js"></script>	


<script>
	
	var app = new Vue({

		el:'#app',
		data:{

			message:'hello vue world'
		}
	})
</script>
</body>
</html>