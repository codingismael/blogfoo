@extends('layouts.frontend')


@section('stunningheader')

 <div class="stunning-header stunning-header-bg-lightviolet">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">{{$mysearch}}</h1>
    </div>
</div>
@endsection

@section('contenu')

<div class="container">
    <div class="row medium-padding120">
  <main class="main">
            
            <div class="row">
                        <div class="case-item-wrap">
                        	@foreach($resultposts as $posted)
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="case-item">
                                    <div class="case-item__thumb">
                                        <img src="{{$posted->featured}}" alt="our case">
                                    </div>
                                    <h6 class="case-item__title"><a href="{{route('single',['slug'=>$posted->slug])}}">{{$posted->title}}</a></h6>
                                </div>
                            </div>
                            @endforeach

                           
            </div>

            <!-- End Post Details -->
            <br>
            <br>
            <br>
            <!-- Sidebar-->

            @section('alltags')



<div class="col-lg-12">
                <aside aria-label="sidebar" class="sidebar sidebar-right">
                    <div  class="widget w-tags">
                        <div class="heading text-center">
                            <h4 class="heading-title">ALL BLOG TAGS</h4>
                            <div class="heading-line">
                                <span class="short-line"></span>
                                <span class="long-line"></span>
                            </div>
                        </div>

                        <div class="tags-wrap">
                            @foreach($alltags as $tagy)
                            <a href="/tag/{{$tagy->id}}" class="w-tags-item">{{$tagy->tagname}}</a>
                            @endforeach

                        </div>
                    </div>
                </aside>
            </div>

            </main>
    </div>

    @endsection

            <!-- End Sidebar-->

 


@endsection