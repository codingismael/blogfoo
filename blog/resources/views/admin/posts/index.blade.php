   @extends('layouts.admin')



   @section('content')

<div class="card card-default">
	

<div class="card-header"><h4>POSTS </h4></div>

<div class="card-body">





 <table style="width:100%">
  @if($allposts->count() > 0)  
  <tr>
  	<th>Image</th>
    <th>Title</th>
    <th>edit</th>
    <th>delete</th>
  </tr>


  @foreach($allposts as $post)
  <tr>
 	<td><img src="{{$post->featured}}" alt="" width="90px" height="50px" ></td>  	
    <td>{{$post->title}}</td>
    <td><a href="/admin/posts/edit/{{$post->id}}" class="btn btn-xs btn-info">Edit</a></td>
    <td><a href="/admin/posts/delete/{{$post->id}}" class="btn btn-xs btn-danger">Delete</a></td>
  </tr>
  @endforeach

  @else

  NO POSTS YET

  @endif
  
</table> 




 </div>







</div>


   @endsection

   