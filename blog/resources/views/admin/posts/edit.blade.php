@extends('layouts.admin')




@section('content')
<div class="card card-default">
	

<div class="card-header"><h2>Edit the post </h2></div>

<div class="card-body">

<form action="/admin/posts/update/{{$post->id}}" method="post" enctype="multipart/form-data">
	

	@csrf


	  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control"  value="{{$post->title}}" name="title">
    </div>

  	  <div class="form-group">
    <label for="featured">Image</label>
    <input type="file" class="form-control"  name="featured">
    </div>

    <div class="form-group">
    <label for="category_id">Categories</label>
     <select id="category_id" name="category_id" class="form-control">
      @foreach($allcategories as $category)
      <option value="{{$category->id}}"   @if($post->category_id == $category->id) {{'selected="selected"'}}   @endif         >{{$category->name}}</option>
      @endforeach
    </select> 
  </div>
    
  <div class="form-group">
  @foreach($alltags as $onetag)
    <input type="checkbox" name="tags[]" value="{{$onetag->id}}"@foreach($post->tags as $t) @if($onetag->id == $t->id) checked @endif @endforeach > {{$onetag->tagname}} <br>
  @endforeach
</div>

  	  <div class="form-group">
    <label for="content">Content</label>
     <textarea rows="10" cols="5" name="content" id="summernote" class="form-control">{{$post->content}}</textarea> 
  </div>



<div class="form-group">
	<div class="text-center">
<button class="btn btn-success" type="submit" value="Submit">submit</button>
</div></div>

</form>

@include('includes.errors')


 </div>







</div>

@endsection

@section('css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

@endsection



@section('scripts')

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>

@endsection