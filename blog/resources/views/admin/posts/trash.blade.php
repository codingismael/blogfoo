   @extends('layouts.admin')



   @section('content')

<div class="card card-default">
	

<div class="card-header"><h4>TRASH  </h4></div>

<div class="card-body">





 <table style="width:100%">

  @if($trashed->count() >0)
  <tr>
  	<th>Image</th>
    <th>Title</th>
    <th>edit</th>
    <th>Restore</th>
    <th>Delete Permanently</th>
  </tr>
  @foreach($trashed as $post)
  <tr>
 	<td><img src="{{$post->featured}}" alt="" width="90px" height="50px"></td>  	
    <td>{{$post->title}}</td>
    <td><a href="/admin/posts/edit/{{$post->id}}" class="btn btn-xs btn-info">Edit</a></td>
    <td><a href="/admin/posts/restore/{{$post->id}}" class="btn btn-xs btn-danger">Restore</a></td>
    <td><a href="/admin/posts/supertrash/{{$post->id}}" class="btn btn-xs btn-danger">Delete Permanently</a></td>
  </tr>
  @endforeach

  @else


  PAS DE POSTS DANS LE TRASH

  @endif
  
</table> 




 </div>







</div>


   @endsection