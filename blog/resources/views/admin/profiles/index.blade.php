@extends('layouts.admin')




@section('content')
<div class="card card-default">
  

<div class="card-header"><h2>My Profile </h2></div>

<div class="card-body">

<form action="/admin/profiles/update" method="post" enctype="multipart/form-data">
  

  @csrf


    <div class="form-group">
    <label for="title">Name</label>
    <input type="text" class="form-control"  value="{{$user->name}}" name="name">
    </div>

        <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control"  value="{{$user->email}}" name="email">
    </div>


    <div class="form-group">
    <label for="email">Password</label>
    <input type="password" class="form-control"  name="password">
    </div>

      <div class="form-group">
    <label for="avatar">Image</label>
    <input type="file" class="form-control"  name="avatar">
    </div> 


        <div class="form-group">
    <label for="title">facebook</label>
    <input type="text" class="form-control"  value="{{$user->profile->facebook}}" name="facebook">
    </div>

            <div class="form-group">
    <label for="title">youtube</label>
    <input type="text" class="form-control"  value="{{$user->profile->youtube}}" name="youtube">
    </div>
    
  

      <div class="form-group">
    <label for="content">About</label>
     <textarea rows="5" cols="5" name="about" id="summernote" class="form-control">{{$user->profile->about}}</textarea> 
  </div>



<div class="form-group">
  <div class="text-center">
<button class="btn btn-success" type="submit" value="Submit">submit</button>
</div></div>

</form>

@include('includes.errors')


 </div>







</div>

@endsection

@section('css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

@endsection



@section('scripts')

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>

@endsection