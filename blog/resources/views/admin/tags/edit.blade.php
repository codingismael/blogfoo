@extends('layouts.admin')




@section('content')
<div class="card card-default">
	

<div class="card-header"><h2>edit tag </h2></div>

<div class="card-body">

<form action="/admin/tags/update/{{$tag->id}}" method="post">
	

	@csrf


<div class="form-group">
                              <label for="name">Name</label>
                              <input type="text" name="name" value="{{$tag->tagname}}" class="form-control">
                        </div>
                        <div class="form-group">
                              <div class="text-center">
                                    <button class="btn btn-success" type="submit">
                                          Edit Tag
                                    </button>
                              </div>
</div>

</form>

@include('includes.errors')


 </div>







</div>

@endsection