   @extends('layouts.admin')



   @section('content')

<div class="card card-default">
	

<div class="card-header"><h2>Tags </h2></div>

<div class="card-body">





 <table style="width:100%">

  @if($alltags->count()>0)
  <tr>
    <th>Tag</th>
    <th>edit</th>
    <th>delete</th>
  </tr>
  @foreach($alltags as $tag)
  <tr>  	
    <td>{{$tag->tagname}}</td>
    <td><a href="/admin/tags/edit/{{$tag->id}}" class="btn btn-xs btn-info">Edit</a></td>
    <td><a href="/admin/tags/delete/{{$tag->id}}" class="btn btn-xs btn-danger">Delete</a></td>
  </tr>
  @endforeach

  @else

Pas de Tags pour le moment

  @endif
  
</table> 




 </div>







</div>


   @endsection