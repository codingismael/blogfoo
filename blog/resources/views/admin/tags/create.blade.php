@extends('layouts.admin')




@section('content')
<div class="card card-default">
	

<div class="card-header"><h2>create a new Tag </h2></div>

<div class="card-body">

<form action="{{route('tags.store')}}" method="post">
	

	@csrf


<div class="form-group">
                              <label for="name">Name</label>
                              <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                              <div class="text-center">
                                    <button class="btn btn-success" type="submit">
                                          Create a Tag
                                    </button>
                              </div>
</div>

</form>

@include('includes.errors')


 </div>







</div>

@endsection