@extends('layouts.admin')




@section('content')
<div class="card card-default">
	

<div class="card-header"><h2>create a new category </h2></div>

<div class="card-body">

<form action="{{route('categories.store')}}" method="post">
	

	@csrf


<div class="form-group">
                              <label for="name">Name</label>
                              <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                              <div class="text-center">
                                    <button class="btn btn-success" type="submit">
                                          Store category
                                    </button>
                              </div>
</div>

</form>

@if($errors->any())

<ul class="list-group">
@foreach($errors->all() as $error)

<li class="list-group-item text-danger">
	
{{$error}}

</li>
@endforeach

</ul>
@endif


 </div>







</div>

@endsection