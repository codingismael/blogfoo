@extends('layouts.admin')




@section('content')
<div class="card card-default">
	

<div class="card-header"><h2>edit category </h2></div>

<div class="card-body">

<form action="/admin/categories/update/{{$category->id}}" method="post">
	

	@csrf


<div class="form-group">
                              <label for="name">Name</label>
                              <input type="text" name="name" value="{{$category->name}}" class="form-control">
                        </div>
                        <div class="form-group">
                              <div class="text-center">
                                    <button class="btn btn-success" type="submit">
                                          Edit Category
                                    </button>
                              </div>
</div>

</form>

@include('includes.errors')


 </div>







</div>

@endsection