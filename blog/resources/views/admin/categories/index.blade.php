   @extends('layouts.admin')



   @section('content')

<div class="card card-default">
	

<div class="card-header"><h2>categories </h2></div>

<div class="card-body">





 <table style="width:100%">

  @if($allcategories->count()>0)
  <tr>
    <th>Category</th>
    <th>edit</th>
    <th>delete</th>
  </tr>
  @foreach($allcategories as $category)
  <tr>  	
    <td>{{$category->name}}</td>
    <td><a href="/admin/categories/edit/{{$category->id}}" class="btn btn-xs btn-info">Edit</a></td>
    <td><a href="/admin/categories/delete/{{$category->id}}" class="btn btn-xs btn-danger">Delete</a></td>
  </tr>
  @endforeach

  @else

Pas de Categories pour le moment

  @endif
  
</table> 

@if($errors->any())

<ul class="list-group">
@foreach($errors->all() as $error)

<li class="list-group-item text-danger">
	
{{$error}}

</li>
@endforeach

</ul>
@endif


 </div>







</div>


   @endsection