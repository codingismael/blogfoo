@extends('layouts.admin')




@section('content')
<div class="card card-default">
	

<div class="card-header"><h2>edit tag </h2></div>

<div class="card-body">

<form action="/admin/settings/update/{{$settings->id}}" method="post">
	

	@csrf


<div class="form-group">
                              <label for="name">Site Name</label>
                              <input type="text" name="sitename" value="{{$settings->sitename}}" class="form-control">
                        </div>

<div class="form-group">
                              <label for="name">contact email</label>
                              <input type="text" name="contact_email" value="{{$settings->contact_email}}" class="form-control">
                        </div>

<div class="form-group">
                              <label for="name">Contact Number</label>
                              <input type="text" name="contact_number" value="{{$settings->contact_number}}" class="form-control">
                        </div>


<div class="form-group">
                              <label for="name">adress</label>
                              <input type="text" name="adress" value="{{$settings->adress}}" class="form-control">
                        </div>


                        <div class="form-group">
                              <div class="text-center">
                                    <button class="btn btn-success" type="submit">
                                          Edit settings
                                    </button>
                              </div>
</div>

</form>

@include('includes.errors')


 </div>







</div>

@endsection