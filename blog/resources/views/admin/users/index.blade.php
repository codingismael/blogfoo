   @extends('layouts.admin')



   @section('content')

<div class="card card-default">
	

<div class="card-header"><h4>USERS </h4></div>

<div class="card-body">





 <table style="width:100%">
  @if($allusers->count() > 0)  
  <tr>
  	<th>Avatar</th>
    <th>Name</th>
    <th>Permissions</th>
    
    <th>delete</th>
  </tr>


  @foreach($allusers as $user)
  <tr>
 	<td><img src="{{asset($user->profile->avatar)}}" alt="" width="50px" height="50px" style="border-radius: 50%;"></td>  	
    <td>{{$user->name}}</td>
    @if ($user->admin == 1)
    <td>@if(Auth::id() != $user->id)<a href="/admin/users/removeadmin/{{$user->id}}" class="btn btn-xs btn-danger">remove permission</a>@endif</td>

    @else
    <td>@if(Auth::id() != $user->id)<a href="/admin/users/giveadmin/{{$user->id}}" class="btn btn-xs btn-info">Make admin</a>@endif</td>
    @endif

    
    <td>@if(Auth::id() != $user->id)<a href="/admin/users/delete/{{$user->id}}" class="btn btn-xs btn-danger">Delete</a>@endif</td>
  </tr>
  @endforeach

  @else

  NO users YET

  @endif
  
</table> 




 </div>







</div>


   @endsection