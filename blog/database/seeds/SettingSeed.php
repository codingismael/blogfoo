<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newsettings = new Setting;

        $newsettings->sitename ='moogi website';
        $newsettings->contact_email='i_idrissi@yahoo.fr';
        $newsettings->contact_number='988 879 555';
        $newsettings->adress='3 avenue du general pilou';

        $newsettings->save();
    }
}
