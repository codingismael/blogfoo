<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontendController@index')->name('index');

Route::get('/coucou', function () {
    return view('coucou');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->prefix('admin')->name('home');



Route::middleware(['auth'])->prefix('admin')->group(function(){


Route::get('/posts/create', 'PostController@create')->name('posts.create');
Route::get('/categories/create', 'CategoryController@create')->name('categories.create');

Route::post('posts/store','PostController@store')->name('posts.store');
Route::post('categories/store','CategoryController@store')->name('categories.store');

Route::get('/posts', 'PostController@index')->name('posts');
Route::get('/categories', 'CategoryController@index')->name('categories');


Route::get('/categories/edit/{category}','CategoryController@edit')->name('categories.edit');
Route::get('/posts/edit/{post}','PostController@edit')->name('posts.edit');

Route::get('/posts/delete/{post}','PostController@destroy')->name('posts.delete');
Route::get('/categories/delete/{category}','CategoryController@destroy')->name('categories.delete');

Route::post('/posts/update/{post}','PostController@update')->name('posts.update');
Route::post('/categories/update/{category}','CategoryController@update')->name('categories.update');

Route::get('/posts/trash','PostController@trash')->name('posts.trash');
Route::get('/posts/supertrash/{post}','PostController@supertrash');

Route::get('posts/restore/{id}','PostController@restore');


Route::get('tags/create','TagController@create')->name('tags.create');
Route::post('tags/store','TagController@store')->name('tags.store');
Route::get('tags/index','TagController@index')->name('tags');
Route::get('tags/edit/{id}','TagController@edit')->name('tags.edit');
Route::get('tags/delete/{id}','TagController@destroy')->name('tags.delete');
Route::post('tags/update/{id}','TagController@update')->name('update.edit');


Route::get('users/index','UserController@index')->name('users');
Route::get('users/create','UserController@create')->name('users.create');
Route::post('users/store','UserController@store')->name('users.store');
Route::get('users/delete/{id}','UserController@destroy')->name('users.delete');


Route::get('users/giveadmin/{id}','UserController@giveadmin')->name('giveadmin');
Route::get('users/removeadmin/{id}','UserController@removeadmin')->name('removeadmin');

Route::get('profiles/index','ProfileController@index')->name('myprofile');
Route::post('profiles/update','ProfileController@update')->name('profiles.update');

Route::get('settings/index','SettingController@index')->name('settings');
Route::post('settings/update/{id}','SettingController@update')->name('settings.update');

});


Route::get('single/{slug}','FrontendController@show')->name('single');

Route::get('category/{id}','FrontendController@showcategory')->name('singlecategory');
Route::get('tag/{id}','FrontendController@showtag')->name('singletag');
Route::get('/results','FrontendController@search')->name('search');

Route::post('/subscribe','NewsletterController@subscribe');

