<?php

namespace App;

use File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{



		use SoftDeletes;


        public function category(){


    			return $this->belongsTo('App\Category');
    }


        protected $fillable = [
        'user_id','title','content','category_id','featured','slug'
    ];



    protected $dates = ['deleted_at'];



    public function getFeaturedAttribute($value){


    		return asset($value);

    }

    public function tags(){


    	return $this->belongsToMany('App\Tag');
    }

    public function user(){

        return $this->belongsTo('App\User');
    }





}
