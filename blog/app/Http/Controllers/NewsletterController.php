<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Newsletter;
use Session;
use App\Post;
use App\Tag;
use App\Category;
use App\Setting;

class NewsletterController extends Controller
{
    public function subscribe(Request $request){

        $request->validate([

        	'email'=>'email',

        ]);



        

		Newsletter::subscribe($request->email);



		


    	return back();
    }
}
