<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Post;
use App\Tag;
use App\User;
use Session;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allposts = Post::orderBy('created_at', 'desc')->get();

        return view('admin.posts.index',compact('allposts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $allcategories = Category::all();
        $alltags = Tag::all();

        if(count($allcategories)==0){

            Session::flash('info','please create categories before creating a post');
            return redirect('admin/categories/create');
        }
        return view('admin.posts.create',compact('allcategories','alltags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      /*  dd($request->all());*/
        $request->validate([

                'title'=>'required',
                'featured'=>'required|image',
                'content'=>'required',
                'category_id'=>'required',


        ]);

        

        

        $featured = $request->featured;

        $featuredNewName = time().$featured->getClientOriginalName();

        $featured->move('uploads/posts',$featuredNewName);




        $newpost = Post::create([


            'title'=>$request->title,
            'slug'=>str_slug($request->title),
            'content'=>$request->content,
            'category_id'=>$request->category_id,
            
            'featured'=>'uploads/posts/'.$featuredNewName,
            'user_id'=> Auth::id()




        ]);
        $newpost->tags()->sync($request->tags);

        Session::flash('success','post created succesfully');

        return redirect('admin/posts');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {

        $allcategories = Category::all();
        $alltags = Tag::all();

        return view('admin.posts.edit',compact('post','allcategories','alltags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {

        $request->validate([

            'title'=>'required',
            'content'=>'required',
            'category_id'=>'required',
        ]);
        

        if($request->hasFile('featured')){

            $featured = $request->featured;
            $newFeaturedName = time().$featured->getClientOriginalName();
            
            $featured->move('uploads/posts/',$newFeaturedName);
            $post->featured = 'uploads/posts/'.$newFeaturedName;
        }

        $tags = $request->tags;

        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        
        $post->save();
        $post->tags()->sync($tags);

        return redirect('admin/posts');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
       return redirect('admin/posts');
    }


    public function trash(){

        $trashed = Post::onlyTrashed()->get();

        return view('admin/posts/trash',compact('trashed'));
    }


    public function deleteForever(Post $post){

        dd("oki");

        $post->forceDelete();

        return redirect('admin/posts/trash');
    }

    public function supertrash($id){




        $postToSuperTrash = Post::withTrashed()->where('id',$id)->first();

        $path_parts = pathinfo($postToSuperTrash->featured);

        // dd($path_parts['basename']);

        $path = $path_parts['basename'];

                if(file_exists('uploads/posts/'.$path)){
        @unlink('uploads/posts/'.$path);

        
    }

        $postToSuperTrash->forceDelete();

        return redirect('admin/posts/trash');

    }

    public function restore($id){

        $postToRestore =Post::withTrashed()->where('id',$id);
        $postToRestore->restore();

        return redirect('admin/posts');


    }
}
