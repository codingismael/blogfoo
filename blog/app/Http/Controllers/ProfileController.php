<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('admin.profiles.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $user = Auth::user();

       $request->validate([

            'name'=>'required',
            'email'=>'required|email',
            'facebook'=>'url',
            'youtube'=>'url',
            


        ]);

       $user->name = $request->name;
       $user->email = $request->email;

       $user->profile->facebook = $request->facebook;
       $user->profile->youtube = $request->youtube;
       $user->profile->about = $request->about;

       if($request->hasFile('avatar')){

            $avatar = $request->avatar ;

            $newavatarname = time().$avatar->getClientOriginalName();

            $avatar->move('uploads/avatar',$newavatarname);

            $user->profile->avatar = 'uploads/avatar/'.$newavatarname;

            $user->profile->save();

       }

       $user->name = $request->name;
       $user->email = $request->email;

       if($request->has('password')){

        $user->password = bcrypt($request->password);

       }

       $user->profile->facebook = $request->facebook;
       $user->profile->youtube = $request->youtube;
       $user->profile->about = $request->about;

       $user->save();
       $user->profile->save();

       return redirect(route('myprofile'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
