<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Post;

class UserController extends Controller
{

    public function __construct(){


        $this->middleware('admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allusers = User::all();

        return view('admin/users/index',compact('allusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'name'=>'required',
            'email'=>'required',
            


        ]);

        $newuser = new User;
        $newuser->name = $request->name;
        $newuser->email = $request->email;
        $newuser->password = bcrypt('123123');

        $newuser->save();

        $newProfile = new Profile;
        $newProfile->user_id = $newuser->id;
        $newProfile->avatar ='uploads/avatars/1.jpg';
        $newProfile->save();


        return redirect('admin/users/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usero = User::find($id);
        $allpostsrelated = Post::where('user_id', $id);
        $allpostsrelated->forceDelete();

        $usero->delete();

        $usero->profile->delete();

        

        return redirect('admin/users/index');
    }

    public function giveadmin($id){

        $theuser =User::find($id);

        $theuser->admin = 1;

        $theuser->save();

        return redirect('admin/users/index');

    }


    public function removeadmin($id){

        $theuser =User::find($id);

        $theuser->admin = 0;

        $theuser->save();

        return redirect('admin/users/index');

    }

    public function getId()
{
  return $this->id;
}



}
