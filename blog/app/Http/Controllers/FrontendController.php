<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Setting;
use App\Category;
use App\Post;
use App\Tag;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::first();
        $categories = Category::take(5)->get();
        $featuredposts = Post::orderBy('created_at', 'desc')->take(3)->get();

        $javascriptposts = Category::find(2);
        $tutorialposts = Category::find(4);





/*        $categoryposts = Post::whereIn('category_id',[2,3,4])->get();

        $javascriptcategory = $categoryposts->where('category_id','2')->take(3);*/




       /* dd($categories);*/
        
        return view('index')->with('settings',$settings)
                            ->with('categories',$categories)
                            ->with('featuredposts',$featuredposts)
                            ->with('javascriptposts',$javascriptposts)
                            ->with('tutorialposts',$tutorialposts)

                            

        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        /*dd($id);*/
        $thepost = Post::where('slug',$slug)->first();
        $settings = Setting::first();
        $categories = Category::take(5)->get();
        $alltags = Tag::all();
        $nextpost = DB::table('posts')->where('id','>',$thepost->id)->min('id');
        $previouspost = DB::table('posts')->where('id','<',$thepost->id)->max('id');
        
        

        return view('single')->with('settings',$settings)
                             ->with('categories',$categories)
                             ->with('post',$thepost)
                             ->with('alltags',$alltags)
                             ->with('nextpost',Post::find($nextpost))
                             ->with('previouspost',Post::find($previouspost))

                            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showcategory($id){

        $categories = Category::take(5)->get();
        $settings = Setting::first();
         $alltags = Tag::all();


        return view('category')->with('categories',$categories)
                               ->with('settings',$settings)
                               ->with('singlecateg',Category::find($id))
                               ->with('alltags',$alltags)
        ;
    }


    public function showtag($id){

        $categories = Category::take(5)->get();
        $settings = Setting::first();
         $alltags = Tag::all();


         return view('tag')->with('categories',$categories)
                               ->with('settings',$settings)
                               ->with('singletag',Tag::find($id))
                               ->with('alltags',$alltags)
        ;

    }


    public function search(){


        $mysearch = request('query');

        
      $posts =  DB::table('posts')->where('title','like',"%".request('query')."%")->get();

      $categories = Category::take(5)->get();
        $settings = Setting::first();
         $alltags = Tag::all();

        



        return view('result',compact('posts'))->with('categories',$categories)
                                              ->with('settings',$settings)
                                              ->with('alltags',$alltags)
                                              ->with('mysearch',$mysearch)
                                              ->with('resultposts',$posts);

    }
}
